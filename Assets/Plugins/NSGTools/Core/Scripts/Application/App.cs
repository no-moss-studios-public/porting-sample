using System.Collections;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using UnityEngine;

namespace NSG.Core
{
    public class App : MonoBehaviour
    {
        public static ServiceContainer Services => Instance._services;
        public static bool IsInitialized => Instance != null;
        
        private static App Instance;

        private ServiceContainer _services;

        private static string ApplicationLocation = "Application";

        public static async UniTask Initialize()
        {
            if (Instance == null)
            {
                InitializeApplication();
                await Instance.InitializeServices();
            }
        }

        private static void InitializeApplication()
        {
            //Create our application
            App prefab = Resources.Load<App>(ApplicationLocation);
            Instance = Instantiate(prefab);
            DontDestroyOnLoad(Instance);
        }

        private async UniTask InitializeServices()
        {
            _services = new ServiceContainer();
            await _services.InitializeChildServices(transform);
        }
    }
}
