using System;
using System.Collections;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using UnityEngine;

namespace NSG.Core
{
    public abstract class SceneController : MonoBehaviour
    {
        private void Start()
        {
            InitializeScene().Forget();   
        }

        private async UniTask InitializeScene()
        {
            if (!App.IsInitialized)
            {
                await App.Initialize();
            }

            OnSceneStart();
        }

        protected abstract void OnSceneStart();
    }
}
