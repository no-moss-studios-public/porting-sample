using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using UnityEngine.SceneManagement;

namespace NSG.Core
{
    public abstract class SceneService : MonoBehaviourService
    {
        protected async UniTask LoadScene(string sceneName)
        {
            //Load the scene
            SceneManager.LoadScene(sceneName);
        }
    }
}
