using System;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using UnityEngine;

namespace NSG.Core
{
    public class ServiceContainer
    {
        private Dictionary<Type, IService> _container;
        
        public ServiceContainer()
        {
            _container = new Dictionary<Type, IService>();
        }

        public async UniTask InitializeChildServices(Transform parent)
        {
            IService[] services = parent.GetComponentsInChildren<IService>();

            List<UniTask> initializes = new List<UniTask>();
            for (int i = 0; i < services.Length; i++)
            {
                _container.Add(services[i].GetType(), services[i]);
                initializes.Add(services[i].InitializeWhenAvailable(this));
            }

            await UniTask.WhenAll(initializes);
        }

        public bool Has<T>() where T : class, IService
        {
            return _container.ContainsKey(typeof(T));
        }

        public T Get<T>() where T : class, IService
        {
            if (_container.ContainsKey(typeof(T)))
            {
                return _container[typeof(T)] as T;
            }
            else
            {
                Debug.LogWarning($"Service not found for type {typeof(T)}");
                return null;
            }
        }

        public async UniTask Add<T>(T service) where T : class, IService
        {
            if (_container.ContainsKey(typeof(T)))
            {
                Debug.LogWarning($"Adding duplicate service for type {typeof(T)} - ignored");
            }
            else
            {
                _container.Add(typeof(T), service);
                await service.InitializeWhenAvailable(this);
            }
        }
    }
}
