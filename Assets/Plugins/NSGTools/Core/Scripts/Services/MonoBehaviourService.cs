using Cysharp.Threading.Tasks;
using UnityEngine;

namespace NSG.Core
{
    public class MonoBehaviourService : MonoBehaviour, IService
    {
        private bool _initialized = false;
        
        public bool IsInitialized()
        {
            return _initialized;
        }
        
        async UniTask IService.WaitUntilCanInitialize(ServiceContainer container)
        {
            await UniTask.WaitUntil(() => CanInitialize(container));
        }

        async UniTask IService.InitializeService(ServiceContainer container)
        {
            await Initialize(container);
            _initialized = true;
        }

        protected virtual bool CanInitialize(ServiceContainer container)
        {
            return true;
        }

        protected virtual UniTask Initialize(ServiceContainer container)
        {
            return UniTask.CompletedTask;
        }
    }
}
