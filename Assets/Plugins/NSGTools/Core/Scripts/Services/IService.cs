using System;
using Cysharp.Threading.Tasks;

namespace NSG.Core
{
    public interface IService
    {
        public async UniTask InitializeWhenAvailable(ServiceContainer container)
        {
            await WaitUntilCanInitialize(container);
            await InitializeService(container);
        }

        public UniTask WaitUntilCanInitialize(ServiceContainer container);

        public UniTask InitializeService(ServiceContainer container);
    }
}
