using System.Collections;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using NSG.Core;
using UnityEngine;

namespace PortosAdventure
{
    public class SceneService : NSG.Core.SceneService
    {
        public enum SceneKey
        {
            Boot = 0,
            Menu = 1,
            LevelOne = 2,
            LevelTwo = 3,
        };

        private Dictionary<SceneKey, string> _sceneDict;

        protected override UniTask Initialize(ServiceContainer container)
        {
            _sceneDict = new Dictionary<SceneKey, string>();
            _sceneDict.Add(SceneKey.Boot, "Scenes/Boot");
            _sceneDict.Add(SceneKey.Menu, "Scenes/Menu");
            _sceneDict.Add(SceneKey.LevelOne, "Scenes/LevelOne");
            _sceneDict.Add(SceneKey.LevelTwo, "Scenes/LevelTwo");
            return base.Initialize(container);
        }

        public async UniTask LoadScene(SceneKey sceneKey)
        {
            if (_sceneDict.ContainsKey(sceneKey))
            {
                await LoadScene(_sceneDict[sceneKey]);
            }
            else
            {
                Debug.LogError($"No scene bound for scene key {sceneKey}");
            }
        }
    }
}
