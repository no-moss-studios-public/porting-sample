using System;
using System.Collections;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using NSG.Core;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace PortosAdventure
{
    public class MenuController : SceneController
    {
        [SerializeField] private Button _playButton;
        [SerializeField] private Button _optionsButton;
        [SerializeField] private Button _quitButton;

        private void Awake()
        {
            _playButton.onClick.AddListener(PlayGame);
            _optionsButton.onClick.AddListener(ShowOptions);
            _quitButton.onClick.AddListener(QuitGame);
        }

        protected override void OnSceneStart()
        {
            //Select the starting button
            EventSystem.current.SetSelectedGameObject(_playButton.gameObject);
        }

        public void PlayGame()
        {
            switch (App.Services.Get<SaveService>().Data.LevelIndex)
            {
                default:
                case (int)SceneService.SceneKey.LevelOne:
                    //Load level 1
                    App.Services.Get<SceneService>().LoadScene(SceneService.SceneKey.LevelOne).Forget();
                    break;
                case (int)SceneService.SceneKey.LevelTwo:
                    //Load level 2
                    App.Services.Get<SceneService>().LoadScene(SceneService.SceneKey.LevelTwo).Forget();
                    break;
            }
        }

        public void ShowOptions()
        {
            Debug.Log("Showing options");
        }

        public void QuitGame()
        {
            Application.Quit();
        }
    }
}
