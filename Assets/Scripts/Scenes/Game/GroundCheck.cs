using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PortosAdventure
{
    public class GroundCheck : MonoBehaviour
    {
        public bool IsGrounded => _touchingGrounds.Count > 0;
        private List<GameObject> _touchingGrounds = new List<GameObject>();
        
        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Ground"))
            {
                if (!_touchingGrounds.Contains(other.gameObject))
                {
                    _touchingGrounds.Add(other.gameObject);
                }   
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.CompareTag("Ground"))
            {
                if (_touchingGrounds.Contains(other.gameObject))
                {
                    _touchingGrounds.Remove(other.gameObject);
                }   
            }
        }
    }
}
