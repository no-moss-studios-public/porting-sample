using System.Collections;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using NSG.Core;
using UnityEngine;

namespace PortosAdventure
{
    public class WarpPoint : MonoBehaviour
    {
        [SerializeField] private SceneService.SceneKey _targetScene;
        
        public async UniTask ChangeLevel()
        {
            App.Services.Get<SaveService>().Data.LevelIndex = (int)_targetScene;
            await App.Services.Get<SaveService>().Save();
            await App.Services.Get<SceneService>().LoadScene(_targetScene);
        }
    }
}
