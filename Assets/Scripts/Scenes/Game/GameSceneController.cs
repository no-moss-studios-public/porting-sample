using System;
using System.Collections;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using Cysharp.Threading.Tasks.Triggers;
using NSG.Core;
using UnityEngine;

namespace PortosAdventure
{
    public class GameSceneController : SceneController
    {
        [SerializeField] private PlayerController _player;
        
        protected override void OnSceneStart()
        {
            _player.Initialize();

            App.Services.Get<InputService>().OnQuit += Quit;
        }

        private void OnDestroy()
        {
            App.Services.Get<InputService>().OnQuit -= Quit;
        }

        private void Quit()
        {
            App.Services.Get<SceneService>().LoadScene(SceneService.SceneKey.Menu).Forget();
        }
    }
}
