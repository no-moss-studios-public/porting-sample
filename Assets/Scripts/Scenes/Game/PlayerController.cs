using System;
using System.Collections;
using System.Collections.Generic;
using DG.DemiEditor.DeGUINodeSystem;
using NSG.Core;
using UnityEngine;

namespace PortosAdventure
{
    public class PlayerController : MonoBehaviour
    {

        private Rigidbody _rigid;
        private Animator _anim;
        
        [SerializeField] private float _moveSpeed;
        [SerializeField] private float _jumpForce;
        [SerializeField] private float _jumpTorque;
        
        [SerializeField] private GroundCheck _ground;

        [SerializeField] private InteractionCheck _interact;
        
        /*
        * Old input code might look like this

        private void Update()
        {
            Vector2 movement = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
            Move(movement);

            if (Input.GetButtonDown("Jump")) Jump();
            if (Input.GetKeyDown(KeyCode.E)) AttemptInteract();
        }
        
        */

        public void Initialize()
        {
            App.Services.Get<InputService>().OnMove += Move;
            App.Services.Get<InputService>().OnJump += Jump;
            App.Services.Get<InputService>().OnInteract += AttemptInteract;

            _rigid = GetComponent<Rigidbody>();
            _anim = GetComponent<Animator>();
        }

        private void Move(Vector2 movement)
        {
            if (Mathf.Abs(movement.x) >= 0.05f || Mathf.Abs(movement.y) >= 0.05f)
            {
                Vector3 velocity = _rigid.velocity;
                velocity.x = movement.x * _moveSpeed;
                velocity.z = movement.y * _moveSpeed;
                _rigid.velocity = velocity;
            }
        }

        private void Jump()
        {
            if (!_ground.IsGrounded) return;
            
            _rigid.AddForce(Vector3.up * _jumpForce);
            _rigid.AddTorque(Vector3.up * _jumpTorque);
        }

        private void AttemptInteract()
        {
            if (_interact.CanInteract)
            {
                _interact.Interact();
            }
        }

        private void OnDestroy()
        {
            App.Services.Get<InputService>().OnMove -= Move;
            App.Services.Get<InputService>().OnJump -= Jump;
            App.Services.Get<InputService>().OnInteract -= AttemptInteract;
        }
    }
}
