using System;
using System.Collections;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using UnityEngine;

namespace PortosAdventure
{
    public class InteractionCheck : MonoBehaviour
    {
        public bool CanInteract => _warpPoints.Count > 0;
        
        [SerializeField] private CanvasGroup[] _interactionCanvas;
        private List<WarpPoint> _warpPoints = new List<WarpPoint>();

        public void Interact()
        {
            if (!CanInteract) return;
            
            _warpPoints[0].ChangeLevel().Forget();
        }

        private void Start()
        {
            for (int i = 0; i < _interactionCanvas.Length; i++)
            {
                _interactionCanvas[i].alpha = 0f;
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            WarpPoint warp = other.gameObject.GetComponent<WarpPoint>();
            if (warp)
            {
                if (!_warpPoints.Contains(warp))
                {
                    _warpPoints.Add(warp);
                }
            }
            UpdateWarpVisibility();
        }

        private void OnTriggerExit(Collider other)
        {
            WarpPoint warp = other.gameObject.GetComponent<WarpPoint>();
            if (warp)
            {
                if (_warpPoints.Contains(warp))
                {
                    _warpPoints.Remove(warp);
                }
            }
            UpdateWarpVisibility();
        }

        private void UpdateWarpVisibility()
        {
            for (int i = 0; i < _interactionCanvas.Length; i++)
            {
                _interactionCanvas[i].DOKill();
                _interactionCanvas[i].DOFade(CanInteract ? 1f : 0f, 0.3f);
            }
        }

        private void OnDestroy()
        {
            for (int i = 0; i < _interactionCanvas.Length; i++)
            {
                _interactionCanvas[i].DOKill();
            }
        }
    }
}
