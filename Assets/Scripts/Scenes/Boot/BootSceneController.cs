using Cysharp.Threading.Tasks;
using NSG.Core;
using UnityEngine;

namespace PortosAdventure
{
    public class BootSceneController : SceneController
    {
        [SerializeField] private SplashElement[] _splashElements;
        
        protected override void OnSceneStart()
        {
            PerformSplashscreens().Forget();
        }

        private async UniTask PerformSplashscreens()
        {
            //Perform splash scene fades
            for (int i = 0; i < _splashElements.Length; i++)
            {
                await _splashElements[i].PerformFade();
            }
            
            //Transition to menu scene
            await App.Services.Get<SceneService>().LoadScene(SceneService.SceneKey.Menu);
        }
    }
}
