using System;
using System.Collections;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using UnityEngine;

namespace PortosAdventure
{
    public class SplashElement : MonoBehaviour
    {
        [SerializeField] private float _fadeInTime = 1f;
        [SerializeField] private float _lingerTime = 1f;
        [SerializeField] private float _fadeOutTime = 1f;
        [SerializeField] private CanvasGroup _canvasGroup;

        public async UniTask PerformFade()
        {
            _canvasGroup.DOFade(1f, _fadeInTime);
            await UniTask.Delay(TimeSpan.FromSeconds(_fadeInTime));

            await UniTask.Delay(TimeSpan.FromSeconds(_lingerTime));
            
            _canvasGroup.DOFade(0f, _fadeOutTime);
            await UniTask.Delay(TimeSpan.FromSeconds(_fadeOutTime));
        }
    }
}
