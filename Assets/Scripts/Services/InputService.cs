using System;
using System.Collections;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using NSG.Core;
using UnityEngine;
using UnityEngine.InputSystem;

namespace PortosAdventure
{
    public class InputService : MonoBehaviourService
    {
        public Action<Vector2> OnMove;
        public Action OnJump;
        public Action OnInteract;
        public Action OnQuit;
        
        [SerializeField] private InputActionReference _moveAction;
        [SerializeField] private InputActionReference _jumpAction;
        [SerializeField] private InputActionReference _interactAction;
        [SerializeField] private InputActionReference _quitAction;

        private void Update()
        {
            Vector2 movement = Vector2.zero;
            movement = _moveAction.action.ReadValue<Vector2>();

            OnMove?.Invoke(movement);

            if (_jumpAction.action.WasPerformedThisFrame())
            {
                OnJump?.Invoke();
            }

            if (_interactAction.action.WasPerformedThisFrame())
            {
                OnInteract?.Invoke();
            }

            if (_quitAction.action.WasPerformedThisFrame())
            {
                OnQuit?.Invoke();
            }
        }
        
        protected override UniTask Initialize(ServiceContainer container)
        {
            _moveAction.action.Enable();
            _jumpAction.action.Enable();
            _interactAction.action.Enable();
            _quitAction.action.Enable();
            return base.Initialize(container);
        }
    }
}
