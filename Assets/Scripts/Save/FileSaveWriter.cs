using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Cysharp.Threading.Tasks;
using UnityEngine;

namespace PortosAdventure
{
    public class FileSaveWriter : SaveWriter
    {
        private string fileLocation => Path.Combine(Application.persistentDataPath, fileName);
        private string fileName = "SaveData";
        
        public override UniTask<bool> HasSaveData()
        {
            return UniTask.FromResult(File.Exists(fileLocation));
        }

        public override async UniTask Save(SaveData data)
        {
            string json = JsonUtility.ToJson(data);
            await File.WriteAllTextAsync(fileLocation, json).AsUniTask();
        }

        public override async UniTask<SaveData> Load()
        {
            SaveData data;
            bool hasData = await HasSaveData();
            if (hasData)
            {
                string json = await File.ReadAllTextAsync(fileLocation).AsUniTask();
                data = JsonUtility.FromJson<SaveData>(json);
            }
            else
            {
                data = new SaveData();
            }

            return data;
        }
    }
}
