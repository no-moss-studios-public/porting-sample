using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PortosAdventure
{
    [System.Serializable]
    public class SaveData
    {
        public int LevelIndex = 0;
    }
}
