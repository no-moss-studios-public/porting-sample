using System.Collections;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using NSG.Core;
using UnityEngine;

namespace PortosAdventure
{
    public class SaveService : MonoBehaviourService
    {
        public SaveData Data => _data;

        private SaveData _data;

        protected override bool CanInitialize(ServiceContainer container)
        {
            if (!container.Has<SaveWriter>()) return false;
            if (!container.Get<SaveWriter>().IsInitialized()) return false;

            return true;
        }

        protected override async UniTask Initialize(ServiceContainer container)
        {
            //Load our initial data
            _data = await container.Get<SaveWriter>().Load();
        }

        public async UniTask Save()
        {
            await App.Services.Get<SaveWriter>().Save(_data);
        }
    }
}
