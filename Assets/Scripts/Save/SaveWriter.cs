using System.Collections;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using NSG.Core;
using UnityEngine;

namespace PortosAdventure
{
    public abstract class SaveWriter : MonoBehaviourService
    {
        public abstract UniTask<bool> HasSaveData();
        public abstract UniTask Save(SaveData data);
        public abstract UniTask<SaveData> Load();
    }
}
