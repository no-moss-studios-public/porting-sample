using System.Collections;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using NSG.Core;
using UnityEngine;

namespace PortosAdventure
{
    public class PlatformFactoryService : MonoBehaviourService
    {
        protected override async UniTask Initialize(ServiceContainer container)
        {
            #if UNITY_EDITOR

            EditorPlatformService editorPlatform = gameObject.AddComponent<EditorPlatformService>();
            await container.Add(editorPlatform);
            
            #elif UNITY_STANDALONE
            
            StandalonePlatformService editorPlatform = gameObject.AddComponent<StandalonePlatformService>();
            await container.Add(editorPlatform);
            
            #elif UNITY_SWITCH

            SwitchPlatformService editorPlatform = gameObject.AddComponent<SwitchPlatformService>();
            await container.Add(editorPlatform);
            
            #elif UNITY_PS5

            PSPlatformService editorPlatform = gameObject.AddComponent<PSPlatformService>();
            await container.Add(editorPlatform);

            #elif UNITY_GAMECORE
            
            PSPlatformService editorPlatform = gameObject.AddComponent<PSPlatformService>();
            await container.Add(editorPlatform);
            
            #endif
        }
    }
}
