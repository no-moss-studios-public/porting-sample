using System.Collections;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using NSG.Core;
using UnityEngine;

namespace PortosAdventure
{
    public class XboxPlatformService : PlatformService
    {
        protected override async UniTask Initialize(ServiceContainer container)
        {
            //Add the save writer for editor
            XboxFileSaveWriter writer = gameObject.AddComponent<XboxFileSaveWriter>();
            await container.Add<SaveWriter>(writer);
        }
    }
}
