using System.Collections;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using UnityEngine;

namespace PortosAdventure
{
    public class XboxFileSaveWriter : FileSaveWriter
    {
        public override UniTask<bool> HasSaveData()
        {
            //Check if data exists on xbox
            return base.HasSaveData();
        }

        public override UniTask<SaveData> Load()
        {
            //Load data on xbox
            return base.Load();
        }

        public override UniTask Save(SaveData data)
        {
            //Save data on xbox
            return base.Save(data);
        }
    }
}
