using System.Collections;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using NSG.Core;
using UnityEngine;

namespace PortosAdventure
{
    public class StandalonePlatformService : PlatformService
    {
        protected override async UniTask Initialize(ServiceContainer container)
        {
            //Add the save writer for standalone
            FileSaveWriter writer = gameObject.AddComponent<FileSaveWriter>();
            await container.Add<SaveWriter>(writer);
        }
    }
}
