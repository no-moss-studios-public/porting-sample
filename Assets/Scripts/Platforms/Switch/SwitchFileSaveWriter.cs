using System.Collections;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using UnityEngine;

namespace PortosAdventure
{
    public class SwitchFileSaveWriter : FileSaveWriter
    {
        public override UniTask<bool> HasSaveData()
        {
            //Perform Switch specific save things
            return base.HasSaveData();
        }

        public override UniTask Save(SaveData data)
        {
            //Perform Switch specific save things
            return base.Save(data);
        }

        public override UniTask<SaveData> Load()
        {
            //Perform Switch specific load things
            return base.Load();
        }
    }
}
