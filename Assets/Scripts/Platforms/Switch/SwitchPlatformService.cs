using System.Collections;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using NSG.Core;
using UnityEngine;

namespace PortosAdventure
{
    public class SwitchPlatformService : PlatformService
    {
        protected override async UniTask Initialize(ServiceContainer container)
        {
            //Add the save writer for switch
            SwitchFileSaveWriter writer = gameObject.AddComponent<SwitchFileSaveWriter>();
            await container.Add<SaveWriter>(writer);
        }
    }
}
