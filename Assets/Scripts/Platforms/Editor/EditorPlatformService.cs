using System.Collections;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using NSG.Core;

namespace PortosAdventure
{
    public class EditorPlatformService : PlatformService
    {
        protected override async UniTask Initialize(ServiceContainer container)
        {
            //Add the save writer for editor
            FileSaveWriter writer = gameObject.AddComponent<FileSaveWriter>();
            await container.Add<SaveWriter>(writer);
        }
    }
}
