using System.Collections;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using UnityEngine;

namespace PortosAdventure
{
    public class PSFileSaveWriter : FileSaveWriter
    {
        public override UniTask<bool> HasSaveData()
        {
            //Do PS specific data checks
            return base.HasSaveData();
        }

        public override UniTask<SaveData> Load()
        {
            //Do PS specific data load
            return base.Load();
        }

        public override UniTask Save(SaveData data)
        {
            //Do PS specific data save
            return base.Save(data);
        }
    }
}
