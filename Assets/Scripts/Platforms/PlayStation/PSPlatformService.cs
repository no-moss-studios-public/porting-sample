using System.Collections;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using NSG.Core;
using UnityEngine;

namespace PortosAdventure
{
    public class PSPlatformService : PlatformService
    {
        protected override async UniTask Initialize(ServiceContainer container)
        {
            //Add the save writer for editor
            PSFileSaveWriter writer = gameObject.AddComponent<PSFileSaveWriter>();
            await container.Add<SaveWriter>(writer);
        }
    }
}
