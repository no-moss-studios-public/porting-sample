# Porting Sample

This project was developed to be used as an example of what a clean porting project might look like.

It was developed for instructional purposes as part of a talk given by Noble Steed Games.

[Find that talk here](https://www.youtube.com/@noblesteedau)